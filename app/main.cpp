/*
  Copyright (C) 2024 UBports Foundation.

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License 3 as published by
  the Free Software Foundation.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see http://www.gnu.org/licenses/.

  Authors: Michael Zanetti <michael.zanetti@canonical.com>
           Riccardo Padovani <rpadovani@ubuntu.com>
           David Planella <david.planella@ubuntu.com>
*/

#include <QtGui/QGuiApplication>
#include <QtQuick/QQuickView>
#include <QtQml/QtQml>
#include <QLibrary>
#include <QDir>

#include <QDebug>

#include <libintl.h>

#include "config.hpp"

int main(int argc, char *argv[])
{
    QGuiApplication::setOrganizationName(QStringLiteral(""));
    // applicationName for click packages (used as an unique app identifier)
    QGuiApplication::setApplicationName("clock.ubports");
    QGuiApplication::setApplicationVersion(AppVersion());
    QGuiApplication a(argc, argv);
    QQmlApplicationEngine engine;
    QString dataFolder(QStandardPaths::writableLocation(QStandardPaths::DataLocation));
    engine.setOfflineStoragePath(dataFolder);
    QGuiApplication::setOrganizationDomain(QGuiApplication::applicationName());

    textdomain("lomiri-clock-app");
    std::string i18nDirectory = I18nDir().toStdString();
    bindtextdomain("lomiri-clock-app", i18nDirectory.c_str());
    bind_textdomain_codeset("lomiri-clock-app", "UTF-8");

    // Set up import paths
    QStringList importPathList = engine.importPathList();
    importPathList.prepend(ImportsDir());

    QStringList args = a.arguments();
    if (args.contains("-h") || args.contains("--help")) {
        qDebug() << "usage: " + args.at(0) + " [-h|--help] [-I <path>] [-q <qmlfile>]";
        qDebug() << "    -h|--help     Print this help.";
        qDebug() << "    -I <path>     Give a path for an additional QML import directory. May be used multiple times.";
        qDebug() << "    -q <qmlfile>  Give an alternative location for the main qml file.";
        return 0;
    }

    QString qmlfile;
    for (int i = 0; i < args.count(); i++) {
        if (args.at(i) == "-I" && args.count() > i + 1) {
            QString addedPath = args.at(i+1);
            if (addedPath.startsWith('.')) {
                addedPath = addedPath.right(addedPath.length() - 1);
                addedPath.prepend(QDir::currentPath());
            }
            importPathList.append(addedPath);
        } else if (args.at(i) == "-q" && args.count() > i + 1) {
            qmlfile = args.at(i+1);
        }
    }

    if (args.contains(QLatin1String("-testability")) || getenv("QT_LOAD_TESTABILITY")) {
        QLibrary testLib(QLatin1String("qttestability"));
        if (testLib.load()) {
            typedef void (*TasInitialize)(void);
            TasInitialize initFunction = (TasInitialize)testLib.resolve("qt_testability_init");
            if (initFunction) {
                initFunction();
            } else {
                qCritical("Library qttestability resolve failed!");
            }
        } else {
            qCritical("Library qttestability load failed!");
        }
    }

    engine.rootContext()->setContextProperty("iconPath", IconPath());

    engine.setImportPathList(importPathList);

    // load the qml file
    if (qmlfile.isEmpty()) {
        QStringList paths = QStandardPaths::standardLocations(QStandardPaths::DataLocation);
        paths.prepend(QDir::currentPath());
        paths.prepend(QCoreApplication::applicationDirPath());
        paths.prepend(ClockDir());

        foreach (const QString &path, paths) {
            QFileInfo fi(path + "/lomiri-clock-app.qml");
            qDebug() << "Trying to load QML from:" << fi.filePath();
            if (fi.exists()) {
                qmlfile = fi.filePath();
                break;
            }
        }
    }
    qDebug() << "using main qml file from:" << qmlfile;
    engine.load(QUrl::fromLocalFile(qmlfile));
    QQuickWindow *window = qobject_cast<QQuickWindow *>(engine.rootObjects().value(0));
    window->show();
    return a.exec();
}
