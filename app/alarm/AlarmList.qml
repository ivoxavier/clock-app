/*
 * Copyright (C) 2014-2016 Canonical Ltd.
 *
 * This file is part of Lomiri Clock App
 *
 * Lomiri Clock App is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Lomiri Clock App is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import Lomiri.Components 1.3
import U1db 1.0 as U1db

LomiriListView {
    id: alarmListView
    objectName: "alarmListView"

    property var localTime
    property bool showAlarmFrequency: true

    signal clearSelection()
    signal closeSelection()
    signal selectAll()

    clip: true
    pressDelay: 75
    currentIndex: -1

    Timer {
        id: alarmTimer
        running: alarmListView.visible && alarmListView.model.count !== 0
        interval: 5000
        repeat: true
        onTriggered: {
            showAlarmFrequency = !showAlarmFrequency
        }
    }

    Transition {
        id:itemTransition
        LomiriNumberAnimation { property: "y"; duration: LomiriAnimation.BriskDuration }
        LomiriNumberAnimation { property: "opacity";  from:0; to:1; duration: LomiriAnimation.BriskDuration }
    }

    addDisplaced: itemTransition
    removeDisplaced: itemTransition
    moveDisplaced: itemTransition

    delegate: AlarmDelegate {
        id: alarmDelegate
        objectName: "alarm" + index

        localTime: alarmListView.localTime
        showAlarmFrequency: alarmListView.showAlarmFrequency

        leadingActions: ListItemActions {
            actions: [
                Action {
                    id: deleteAction
                    objectName: "deleteAction"
                    iconName: "delete"
                    text: i18n.tr("Delete")
                    onTriggered: {
                        alarmDelegate.enabled = false;
                        model.cancel();
                    }
                }
            ]
        }

        visible: !activeTimers.isAlarmATimerAlarm(model)

        onClicked: {
            if (selectMode) {
                selected = !selected
            } else {
                editAlarm(model);
            }
        }

        onPressAndHold: {
            ListView.view.ViewItems.selectMode = !ListView.view.ViewItems.selectMode
        }
    }

    onClearSelection: {
        ViewItems.selectedIndices = []
    }

    onSelectAll: {
        var tmp = []

        for (var i=0; i < model.count; i++) {
            tmp.push(i)
        }

        ViewItems.selectedIndices = tmp
    }

    onCloseSelection: {
        clearSelection()
        ViewItems.selectMode = false
    }
    
    //-------------------------- Functions -------------------
    
    function editAlarm(alarm) {
        mainStack.push(Qt.resolvedUrl("EditAlarmPage.qml"), {isNewAlarm: false, tempAlarm: alarm, alarmModel: alarmModel})
    }
}

